# README #

clone this repo

cd to the cloned repo
```
git clone https://github.com/docker/dockercloud-hello-world.git
mv dockercloud-hello-world/* .
vagrant up
```

if you get this:
```
==> docker: Waiting for machine to boot. This may take a few minutes...
    docker: SSH address: 127.0.0.1:2222
    docker: SSH username: vagrant
    docker: SSH auth method: private key
    docker: Warning: Remote connection disconnect. Retrying...
    docker: Warning: Connection reset. Retrying...
```
It's safe to ignore, that's just the network interface coming up.